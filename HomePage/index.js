(function() {
  // Instance material components
  const MDCSelect = mdc.select.MDCSelect;
  const selects = document.querySelectorAll('.mdc-select');
  const dmrSelect = new MDCSelect(selects[0]);
  const dmcsSelect = new MDCSelect(selects[1]);
  const admrSelect = new MDCSelect(selects[0]);
  const admcsSelect = new MDCSelect(selects[1]);
  // Vars
  let segment = 'products';
  let section = 'industry';
  const dmrValues = [
    { plan: '1', members: '5 Members', price: 'Free Forever', costPerMY: '0' },
    { plan: '2', members: '50', price: '400', costPerMY: '8' },
    { plan: '3', members: '100', price: '600', costPerMY: '6' },
    { plan: '4', members: '250', price: '1250', costPerMY: '5' },
    { plan: '5', members: '500', price: '1500', costPerMY: '3' },
    { plan: '6', members: '750', price: '2250', costPerMY: '3' },
    { plan: '7', members: '1000', price: '2000', costPerMY: '2' },
    { plan: '8', members: '1500', price: '3000', costPerMY: '2' },
    { plan: '9', members: '2500', price: '5000', costPerMY: '2' },
    { plan: '10', members: '5000', price: '5000', costPerMY: '1' },
    { plan: '11', members: '7500', price: '7500', costPerMY: '1' },
    { plan: '12', members: '10000', price: '10000', costPerMY: '1' },
    { plan: '13', members: '10000+', price: 'Contact us', costPerMY: '' }
  ];
  const dmcsValues = [
    { plan: '1', members: '50 Members', price: '400', costPerMY: '12' },
    { plan: '2', members: '100', price: '1200', costPerMY: '12' },
    { plan: '3', members: '250', price: '3000', costPerMY: '12' },
    { plan: '4', members: '500', price: '5000', costPerMY: '10' },
    { plan: '5', members: '750', price: '7500', costPerMY: '10' },
    { plan: '6', members: '1000', price: '10000', costPerMY: '10' },
    { plan: '7', members: '1500', price: '12000', costPerMY: '8' },
    { plan: '8', members: '2500', price: '20000', costPerMY: '8' },
    { plan: '9', members: '5000', price: '40000', costPerMY: '8' },
    { plan: '10', members: '7500', price: '60000', costPerMY: '8' },
    { plan: '11', members: '10000', price: '80000', costPerMY: '8' },
    { plan: '12', members: '10000+', price: 'Contact us', costPerMY: '' }
  ];
  const admrValues = [
    { plan: '1', members: '50 Workers', price: '1,800', costPerMY: '36' },
    { plan: '2', members: '100', price: '3,600', costPerMY: '4' },
    { plan: '3', members: '200', price: '7,200', costPerMY: '4' },
    { plan: '4', members: '400', price: '10,800', costPerMY: '3' },
    { plan: '5', members: '700', price: '18,900', costPerMY: '3' },
    { plan: '6', members: '1000', price: '27,000', costPerMY: '2' },
    { plan: '7', members: '1500', price: '27,000', costPerMY: '2' },
    { plan: '8', members: '2500', price: '45,000', costPerMY: '2' },
    { plan: '9', members: '5000', price: '90,000', costPerMY: '2' },
    { plan: '10', members: '7,500', price: '90,000', costPerMY: '2' },
    { plan: '11', members: '9,999', price: '119,988', costPerMY: '1' },
    { plan: '12', members: '10,000+', price: 'Contact us', costPerMY: '' }
  ];
  const admcsValues = [
    { plan: '1', members: '50 Workers', price: '2,025', costPerMY: '40.5' },
    { plan: '2', members: '100', price: '4,050', costPerMY: '4' },
    { plan: '3', members: '200', price: '8,100', costPerMY: '4' },
    { plan: '4', members: '400', price: '12,600', costPerMY: '3' },
    { plan: '5', members: '700', price: '22,050', costPerMY: '3' },
    { plan: '6', members: '1000', price: '31,500', costPerMY: '2' },
    { plan: '7', members: '1500', price: '33,750', costPerMY: '2' },
    { plan: '8', members: '2500', price: '56,250', costPerMY: '2' },
    { plan: '9', members: '5000', price: '112,500', costPerMY: '2' },
    { plan: '10', members: '7500', price: '101,250', costPerMY: '2' },
    { plan: '11', members: '9,999', price: '134,987', costPerMY: '1' },
    { plan: '12', members: '10,000+', price: 'Contact us', costPerMY: '' }
  ];

  // Elements
  const body = document.getElementById('body');
  const productDropdown = document.getElementById('p_dropdown');
  const companyDropdown = document.getElementById('c_dropdown');
  const hoverContentProducts = document.getElementById('hc_products');
  const hoverContentCompany = document.getElementById('hc_company');
  const collapseMenuProductLink = document.getElementById('cm_products_link');
  const collapseMenuCompanyLink = document.getElementById('cm_company_link');
  const collapseMenuProductSegment = document.getElementById('cm_products');
  const collapseMenuCompanySegment = document.getElementById('cm_company');
  const employersCardContainer = document.getElementById(
    'employers_cards_container'
  );
  const industryCardContainer = document.getElementById(
    'industry_cards_container'
  );
  const agencyCardContainer = document.getElementById(
    'agency_cards_container'
  );
  const menuToggle = document.getElementById('menu-toggle');
  const menu = document.getElementById('collapse-menu');
  const mainContent = document.getElementById('main');
  const employersBtn = document.getElementById('btn_employers');
  const peopleBtn = document.getElementById('btn_people');
  const workersBtn = document.getElementById('btn_workers');
  const dmrCard = document.getElementById('dmr_card');
  const dmrList = dmrCard.getElementsByClassName('mdc-list')[0];
  const dmrPrice = dmrCard.getElementsByTagName('h4')[1];
  const dmrPriceLabel = dmrCard.getElementsByTagName('h6')[0];
  const dmcsCard = document.getElementById('dmcs_card');
  const dmcsList = dmcsCard.getElementsByClassName('mdc-list')[0];
  const dmcsPrice = dmcsCard.getElementsByTagName('h4')[1];
  const dmcsPriceLabel = dmcsCard.getElementsByTagName('h6')[0];
  const admrCard = document.getElementById('admr_card');
  const admrList = admrCard.getElementsByClassName('mdc-list')[0];
  const admrPrice = admrCard.getElementsByTagName('h4')[1];
  const admrPriceLabel = admrCard.getElementsByTagName('h6')[0];

  const admcsCard = document.getElementById('admcs_card');
  const admcsList = admcsCard.getElementsByClassName('mdc-list')[0];
  const admcsPrice = admcsCard.getElementsByTagName('h4')[1];
  const admcsPriceLabel = admcsCard.getElementsByTagName('h6')[0];

  const createPriceOptionEl = list => (v, i) => {
    const el = document.createElement('li');
    if (i === 0) {
      el.setAttribute('aria-selected', 'true');
    }
    el.setAttribute('class', 'mdc-list-item');
    el.setAttribute('data-value', v.plan);
    el.innerText = v.members;
    list.appendChild(el);
  };
  dmrValues.filter(v => v.plan !== '1').map(createPriceOptionEl(dmrList));
  dmcsValues.filter(v => v.plan !== '1').map(createPriceOptionEl(dmcsList));
  admrValues.filter(v => v.plan !== '1').map(createPriceOptionEl(admrList));

  admcsValues.filter(v => v.plan !== '1').map(createPriceOptionEl(admcsList));
  
  dmrSelect.selectedIndex = 0;
  dmrSelect.value = '1';
  dmrSelect.listen('MDCSelect:change', e => {
    const val = dmrValues.find(v => v.plan === dmrSelect.value);
    if (dmrSelect.value === '1' || dmrSelect.value === '13') {
      dmrPrice.innerText = val.price;
      dmrPriceLabel.hidden = true;
    } else {
      dmrPrice.innerText = `AUD $${val.price}`;
      dmrPriceLabel.hidden = false;
    }
  });
  dmcsSelect.selectedIndex = 0;
  dmcsSelect.value = '1';
  dmcsSelect.listen('MDCSelect:change', e => {
    const val = dmcsValues.find(v => v.plan === dmcsSelect.value);
    if (dmcsSelect.value === '12') {
      dmcsPrice.innerText = val.price;
      dmcsPriceLabel.hidden = true;
    } else {
      dmcsPrice.innerText = `AUD $${val.price}`;
      dmcsPriceLabel.hidden = false;
    }
  });
  admrSelect.selectedIndex = 0;
  admrSelect.value = '1';
  admrSelect.listen('MDCSelect:change', e => {
    const val = admrValues.find(v => v.plan === admrSelect.value);
    if (admrSelect.value === '12') {
      admrPrice.innerText = val.price;
      admrPriceLabel.hidden = true;
    } else {
      admrPrice.innerText = `AUD $${val.price}`;
      admrPriceLabel.hidden = false;
    }
  });

  admcsSelect.selectedIndex = 0;
  admcsSelect.value = '1';
  admcsSelect.listen('MDCSelect:change', e => {
    const val = admcsValues.find(v => v.plan === admcsSelect.value);
    if (admcsSelect.value === '12') {
      admcsPrice.innerText = val.price;
      admcsPriceLabel.hidden = true;
    } else {
      admcsPrice.innerText = `AUD $${val.price}`;
      admcsPriceLabel.hidden = false;
    }
  });

  // Navbar dropdowns events
  employersBtn.onclick = () => {
    if (segment !== 'employers') {
      segment = 'employers';
      employersBtn.classList.add(['main__section_first_row_button-selected']);
      peopleBtn.classList.remove(['main__section_first_row_button-selected']);
      workersBtn.classList.remove(['main__section_first_row_button-selected']);
      employersCardContainer.style.display = 'flex';
      industryCardContainer.style.display = 'none';
      agencyCardContainer.style.display = 'none';
    }
  };
  peopleBtn.onclick = () => {
    if (segment !== 'people') {
      segment = 'people';
      employersBtn.classList.remove([
        'main__section_first_row_button-selected'
      ]);
      peopleBtn.classList.add(['main__section_first_row_button-selected']);
      workersBtn.classList.remove(['main__section_first_row_button-selected']);
      employersCardContainer.style.display = 'none';
      agencyCardContainer.style.display = 'none';
      industryCardContainer.style.display = 'flex';
    }
  };
  workersBtn.onclick = () => {
    if (segment !== 'workers') {
      segment = 'workers';
      employersBtn.classList.remove([
        'main__section_first_row_button-selected'
      ]);
      peopleBtn.classList.remove(['main__section_first_row_button-selected']);
      workersBtn.classList.add(['main__section_first_row_button-selected']);
      employersCardContainer.style.display = 'none';
      agencyCardContainer.style.display = 'flex';
      industryCardContainer.style.display = 'none';
    }
  };
  menuToggle.onclick = () => {
    if (menu.style.display === 'block') {
      menu.style.display = 'none';
      mainContent.style.display = 'block';
    } else {
      mainContent.style.display = 'none';
      menu.style.display = 'block';
    }
  };
  productDropdown.onmouseover = () => {
    hoverContentProducts.style.display = 'block';
    hoverContentCompany.style.display = 'none';
  };
  companyDropdown.onmouseover = () => {
    hoverContentProducts.style.display = 'none';
    hoverContentCompany.style.display = 'block';
  };
  productDropdown.onclick = () => {
    if (hoverContentProducts.style.display === 'block') {
      hoverContentProducts.style.display = 'none';
    }
  };
  companyDropdown.onclick = () => {
    if (hoverContentCompany.style.display === 'block') {
      hoverContentCompany.style.display = 'none';
    }
  };
  collapseMenuProductLink.onclick = e => {
    if (segment !== 'products') {
      segment = 'products';
      const pChild = collapseMenuProductLink.firstElementChild;
      const cChild = collapseMenuCompanyLink.firstElementChild;
      pChild.classList.add(['collapse-menu_segments-item-selected']);
      cChild.classList.remove(['collapse-menu_segments-item-selected']);
      collapseMenuProductSegment.style.display = 'block';
      collapseMenuCompanySegment.style.display = 'none';
    }
  };
  collapseMenuCompanyLink.onclick = e => {
    if (segment !== 'company') {
      segment = 'company';
      const pChild = collapseMenuProductLink.firstElementChild;
      const cChild = collapseMenuCompanyLink.firstElementChild;
      cChild.classList.add(['collapse-menu_segments-item-selected']);
      pChild.classList.remove(['collapse-menu_segments-item-selected']);
      collapseMenuCompanySegment.style.display = 'block';
      collapseMenuProductSegment.style.display = 'none';
    }
  };
  body.onclick = e => {
    if (e.clientY <= 465) return;
    if (hoverContentProducts.style.display === 'block') {
      hoverContentProducts.style.display = 'none';
    }
    if (hoverContentCompany.style.display === 'block') {
      hoverContentCompany.style.display = 'none';
    }
  };
  const acc = document.getElementsByClassName('accordion');
  let i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener('click', function() {
      /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
      this.classList.toggle('active');

      /* Toggle between hiding and showing the active panel */
      const panel = this.nextElementSibling;
      if (panel.style.display === 'block') {
        panel.style.display = 'none';
      } else {
        panel.style.display = 'block';
      }
    });
  }


})();

function faq1() {
  showPage("faq_1");
  hidePage("faq_2");
  hidePage("faq_3");
  hidePage("faq_4");
  hidePage("faq_5");
  hidePage("faq_6");
  changeColor("a1");
  changeColorBack("a2");
  changeColorBack("a3");
  changeColorBack("a4");
  changeColorBack("a5");
  changeColorBack("a6");
  
            
}

function faq2() {
  showPage("faq_2");
  hidePage("faq_1");
  hidePage("faq_3");
  hidePage("faq_4");
  hidePage("faq_5");
  hidePage("faq_6");
  changeColor("a2"); 
  changeColorBack("a1");
  changeColorBack("a3");
  changeColorBack("a4");
  changeColorBack("a5");
  changeColorBack("a6");
  
           
}

function faq3() {
  showPage("faq_3");
  hidePage("faq_1");
  hidePage("faq_2");
  hidePage("faq_4");
  hidePage("faq_5");
  hidePage("faq_6");
  changeColor("a3");
  changeColorBack("a1");
  changeColorBack("a2");
  changeColorBack("a4");
  changeColorBack("a5");
  changeColorBack("a6"); 
  
           
}

function faq4() {
  showPage("faq_4");
  hidePage("faq_1");
  hidePage("faq_2");
  hidePage("faq_3");
  hidePage("faq_5");
  hidePage("faq_6");
  changeColor("a4");
  changeColorBack("a1");
  changeColorBack("a2");
  changeColorBack("a3");
  changeColorBack("a5");
  changeColorBack("a6"); 
            
}

function faq5() {
  showPage("faq_5");
  hidePage("faq_1");
  hidePage("faq_2");
  hidePage("faq_3");
  hidePage("faq_4");
  hidePage("faq_6");
  changeColor("a5");
  changeColorBack("a1");
  changeColorBack("a2");
  changeColorBack("a3");
  changeColorBack("a4");
  changeColorBack("a6");          
}

function faq6() {
  showPage("faq_6");
  hidePage("faq_1");
  hidePage("faq_2");
  hidePage("faq_3");
  hidePage("faq_4");
  hidePage("faq_5");
  changeColor("a6");
  changeColorBack("a1");
  changeColorBack("a2");
  changeColorBack("a3");
  changeColorBack("a4");
  changeColorBack("a5");          
}


function showPage(page) {
  document.getElementById(page).style.display = "block";
}
function hidePage(page) {
  document.getElementById(page).style.display = "none";
}
function changeColor(page) {
document.getElementById(page).className = "mdc-theme--primary text-strong main__section_third-link main__section_title-dark";
return false;
}
function changeColorBack(page) {
document.getElementById(page).className = "mdc-theme--primary text-strong main__section_third-link";
return false;
}  


 



  
      
        
    



